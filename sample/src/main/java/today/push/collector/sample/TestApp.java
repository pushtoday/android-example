package today.push.collector.sample;

import android.app.Application;
import today.push.sdk.PushToday;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;


public class TestApp extends Application {
    private Queue<String> notReceivedMessages;

    public Queue<String> getNotReceivedMessages() {
        return notReceivedMessages;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notReceivedMessages = new LinkedBlockingQueue<>();

        PushToday.setLogProvider(new PushToday.LogProvider() {
            @Override
            public void log(String message) {
                notReceivedMessages.add(message);
            }
        });

        PushToday.initialise(this, getString(R.string.app_id), getString(R.string.project_id));
    }
}
