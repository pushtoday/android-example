package today.push.collector.sample;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import today.push.sdk.PushToday;
import today.push.sdk.RequestExecutor;

import java.text.SimpleDateFormat;
import java.util.Date;

public class InfoActivity extends BaseActivity {
    private ScrollView console_SV;
    private TextView log_TV;
    private int localMessageId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        log_TV = (TextView) findViewById(R.id.tv_info);
        console_SV = (ScrollView) findViewById(R.id.sv_info_console);
        log_TV.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf"));

        if(savedInstanceState != null){
            log_TV.setText(savedInstanceState.getString("log", ""));
        }
        String message;
        while ((message = ((TestApp) getApplication()).getNotReceivedMessages().poll()) != null) {
            printMessage(message);
        }
        PushToday.setLogProvider(new PushToday.LogProvider() {
            @Override
            public void log(final String message) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        printMessage(message);
                        Log.d("PUSH TODAY TEST", message);
                    }
                });
            }
        });

        findViewById(R.id.b_info_url_local_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject data = new JSONObject();

                    data.put("id", "" + localMessageId++);
                    data.put("type", "url");
                    data.put("url", "http://push.today");
                    data.put("title", "title");
                    data.put("subtitle", "subtitle");
                    data.put("message", "message");
                    data.put("LED", 0xff00);
                    data.put("vibration", 1);
                    data.put("sound", "url_sound");
                    data.put("icon", "custom_icon");

                    final RequestExecutor request = new RequestExecutor("gcm/send", "POST");
                    request.setHost("https://android.googleapis.com/");
                    request.addHeaders("Authorization", "key=" + getString(R.string.api_key));
                    request.addHeaders("Content-Type", "application/json");
                    JSONObject json = new JSONObject();
                    JSONArray ids = new JSONArray();
                    ids.put(PushToday.getGCMSenderId());
                    json.put("registration_ids", ids);
                    json.put("data", data);


                    request.putPostBody(json.toString());
                    request.execute(new RequestExecutor.Callback() {
                        @Override
                        public void handle(final boolean success) {
                            if (success && getSelfRequest().getCode() == 200) {
                                printMessage("Test push has been sent");
                            } else {
                                printMessage("Error (code " + request.getCode() + ") Try later");
                            }
                        }
                    });
                } catch (JSONException ignored) {
                }
            }
        });
        findViewById(R.id.b_info_msg_local_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject data = new JSONObject();
                    data.put("id", "" + localMessageId++);
                    data.put("type", "message");
                    data.put("url", "http://push.today");
                    data.put("title", "title");
                    data.put("subtitle", "subtitle");
                    data.put("LED", 0x0800);
                    data.put("vibration", 1);
                    data.put("sound", "message_sound");
                    data.put("icon", "custom_icon");

                    final RequestExecutor request = new RequestExecutor("gcm/send", "POST");
                    request.setHost("https://android.googleapis.com/");
                    request.addHeaders("Authorization", "key=" + getString(R.string.api_key));
                    request.addHeaders("Content-Type", "application/json");
                    JSONObject json = new JSONObject();
                    JSONArray ids = new JSONArray();
                    ids.put(PushToday.getGCMSenderId());
                    json.put("registration_ids", ids);
                    json.put("data", data);

                    request.putPostBody(json.toString());
                    request.execute(new RequestExecutor.Callback() {
                        @Override
                        public void handle(final boolean success) {
                            if (success && getSelfRequest().getCode() == 200) {
                                printMessage("Test push has been sent");
                            } else {
                                printMessage("Error (code " + request.getCode() + ") Try later");
                            }
                        }
                    });
                } catch (JSONException ignored) {
                }
            }
        });
        findViewById(R.id.b_info_show_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject data = new JSONObject();
                    data.put("id", "" + localMessageId++);
                    data.put("type", "inapp_message");
                    data.put("title", "title");
                    data.put("subtitle", "subtitle");
                    data.put("LED", 0x00ff);
                    data.put("vibration", 1);
                    data.put("sound", "dialog_sound");
                    data.put("icon", "custom_icon");
                    JSONObject extra = new JSONObject();
                    extra.put("message","Hello world");
                    data.put("extra", extra);

                    final RequestExecutor request = new RequestExecutor("gcm/send", "POST");
                    request.setHost("https://android.googleapis.com/");
                    request.addHeaders("Authorization", "key=" + getString(R.string.api_key));
                    request.addHeaders("Content-Type", "application/json");
                    JSONObject json = new JSONObject();
                    JSONArray ids = new JSONArray();
                    ids.put(PushToday.getGCMSenderId());
                    json.put("registration_ids", ids);
                    json.put("data", data);

                    request.putPostBody(json.toString());
                    request.execute(new RequestExecutor.Callback() {
                        @Override
                        public void handle(final boolean success) {
                            if (success && getSelfRequest().getCode() == 200) {
                                printMessage("Test push has been sent");
                            } else {
                                printMessage("Error (code " + request.getCode() + ") Try later");
                            }
                        }
                    });
                } catch (JSONException ignored) {
                }
            }
        });
        final Button location_B = (Button) findViewById(R.id.b_info_location);
        if (PushToday.isLocationListening()) {
            location_B.setText("Stop geotracking");
        } else {
            location_B.setText("Start geotracking");
        }
        location_B.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PushToday.isLocationListening()) {
                    PushToday.stopListenLocation();
                    location_B.setText("Start geotracking");
                } else {
                    PushToday.startListenLocation(100);
                    location_B.setText("Stop geotracking");
                }
            }
        });

        final Button reg_B = (Button) findViewById(R.id.b_info_unregister);
        reg_B.setText(PushToday.isRegistered() ? "Unregister in GCM" : "Register in GCM");
        reg_B.postDelayed(new Runnable() {
            @Override
            public void run() {
                reg_B.setText(PushToday.isRegistered() ? "Unregister in GCM" : "Register in GCM");
                reg_B.postDelayed(this, 500);
            }
        }, 500);
        reg_B.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PushToday.isRegistered()) {
                    PushToday.unregister();
                    reg_B.setText("Register in GCM");
                } else {
                    PushToday.register();
                    reg_B.setText("Unregister in GCM");
                }
            }
        });
        findViewById(R.id.b_clear_log).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                log_TV.setText("");
            }
        });
        showDialog(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        showDialog(intent);
    }

    private void showDialog(Intent intent){
        if(!intent.hasExtra("extra")) return;
        try {
            JSONObject info = new JSONObject(intent.getStringExtra("extra"));
            new AlertDialog.Builder(this).setMessage(info.getString("message")).setPositiveButton("Ok",null).show();
        } catch (JSONException ignored) {
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("log", log_TV.getText().toString());
        super.onSaveInstanceState(outState);
    }

    private void printMessage(CharSequence message) {
        String time = new SimpleDateFormat("\nHH:mm:ss> ").format(new Date());
        StringBuilder messages = new StringBuilder(log_TV.getText());
        messages.insert(0,time + message + "\n");
        log_TV.setText(messages);
        console_SV.post(new Runnable() {
            @Override
            public void run() {
                console_SV.fullScroll(ScrollView.FOCUS_UP);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PushToday.setLogProvider(null);
    }
}
