package today.push.collector.sample;

import android.app.Activity;
import today.push.sdk.PushToday;

public abstract class BaseActivity extends Activity{

    @Override
    protected void onResume() {
        super.onResume();
        PushToday.resumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        PushToday.paused();
    }
}
